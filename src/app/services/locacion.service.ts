import { Injectable } from '@angular/core';
import { Coordenada } from '../interface/coordenadas.interface';

@Injectable({
  providedIn: 'root'
})
export class LocacionService {
  constructor(
  ) { }
  getPosition(): Promise<Coordenada> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resp => {
        const locacion: Coordenada = {
          lng: resp.coords.longitude,
          lat: resp.coords.latitude
        }
        resolve(locacion);
      },
        err => {
          reject(err);
        });
    });
  }


}
