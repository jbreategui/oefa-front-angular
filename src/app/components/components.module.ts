import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmarModalComponent } from './confirmar-modal/confirmar-modal.component';
import { MapaComponent } from './mapa/mapa.component';
import { MaterialModule } from '../material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DialogMapaComponent } from './dialog-mapa/dialog-mapa.component';
import { AlertaModalComponent } from './alerta-modal/alerta-modal.component';



@NgModule({
  declarations: [
    ConfirmarModalComponent,
    MapaComponent,
    DialogMapaComponent,
    AlertaModalComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
  ],
  exports: [
    ConfirmarModalComponent,
    MapaComponent,
    DialogMapaComponent,
    AlertaModalComponent
  ]
})
export class ComponentsModule { }
