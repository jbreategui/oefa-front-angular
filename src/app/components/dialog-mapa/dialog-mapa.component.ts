import { Component, OnInit, Input, Output, ViewChild, ElementRef, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LocacionService } from '../../services/locacion.service';
import { Coordenada, DialogCoordenada } from '../../interface/coordenadas.interface';

@Component({
  selector: 'app-dialog-mapa',
  templateUrl: './dialog-mapa.component.html',
  styleUrls: ['./dialog-mapa.component.css']
})
export class DialogMapaComponent implements OnInit {


  @ViewChild('map') mapaELement!: ElementRef<HTMLElement>;
  map!: google.maps.Map;
  marcadores: google.maps.Marker[] = [];
  dialogCoordenada: DialogCoordenada = {
    alturaDialog: '',
    coordenadas: {
      lat: -9.189967,
      lng: -75.015152
    },
  };
  constructor(
    private locacion: LocacionService,
    @Inject(MAT_DIALOG_DATA) public coord: DialogCoordenada) {

    this.dialogCoordenada = coord;

  }

  ngOnInit() {

  }
  ngAfterViewInit() {

    this.mapaELement.nativeElement.style.height = this.dialogCoordenada.alturaDialog ? this.dialogCoordenada.alturaDialog : '500px';
    if (this.dialogCoordenada) {
      this.cargarMapa(this.dialogCoordenada.coordenadas);
    } else {
      this.locacion.getPosition().then((r) => {
        const coordenada = r;
        this.cargarMapa(coordenada);
      }).catch(() => {
        const coordenadas = {
          lat: -9.189967,
          lng: -75.015152
        };
        this.cargarMapa(coordenadas);
      })
    }



  }
  cargarMapa(coords: Coordenada) {

    const latLng = new google.maps.LatLng(coords.lat, coords.lng);
    const mapaOpciones: google.maps.MapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
 


    };

    this.map = new google.maps.Map(this.mapaELement.nativeElement, mapaOpciones);
    const marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      draggable:false
 
    });
    google.maps.event.addDomListener(marker, 'dragend', (coors: any) => {

      const nuevoMarcador: Coordenada = {
        lat: coors.latLng.lat(),
        lng: coors.latLng.lng(),
      };

    });
  }
}
