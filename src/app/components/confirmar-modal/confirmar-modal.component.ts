import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RPEAService } from '../../services/rpea.service';

@Component({
  selector: 'app-confirmar-modal',
  templateUrl: './confirmar-modal.component.html',
  styleUrls: ['./confirmar-modal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmarModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ConfirmarModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private wsrepea: RPEAService
  ) { }

  ngOnInit() {
  }
  salir(flag: boolean) {
    this.dialogRef.close(flag);
  }

  registrarRPEA() {
    const rpea = this.data;
    this.wsrepea.registrarRPEA({ ...rpea }).subscribe((res: any) => {
    });
  }
}
