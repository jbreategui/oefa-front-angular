import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { LocacionService } from '../../services/locacion.service';
import { Coordenada } from '../../interface/coordenadas.interface';
@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit, AfterViewInit {
  @Input('altura') altura: string = '';
  @Input('titulo') titulo = true;
  @ViewChild('map') mapaELement!: ElementRef<HTMLElement>;
  @Input('coordenadas') coordenadas!: Coordenada;
  @Output() retornarCoordenadas = new EventEmitter<Coordenada>();

  map!: google.maps.Map;
  marcadores: google.maps.Marker[] = [];




  constructor(
    private locacion: LocacionService) {

  }

  ngOnInit() {

  }
  ngAfterViewInit() {
    this.mapaELement.nativeElement.style.height = this.altura ? this.altura : '500px';
    if (this.coordenadas) {
      this.cargarMapa(this.coordenadas);
    } else {
      this.locacion.getPosition().then((r) => {
        const coordenada = r;
        this.cargarMapa(coordenada);
      }).catch(() => {
        const coordenadas = {
          lat: -9.189967,
          lng: -75.015152
        };
        this.cargarMapa(coordenadas);
      })
    }



  }
  cargarMapa(coords: Coordenada) {

    const latLng = new google.maps.LatLng(coords.lat, coords.lng);
    const mapaOpciones: google.maps.MapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: false,


    };

    this.map = new google.maps.Map(this.mapaELement.nativeElement, mapaOpciones);
    const marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      draggable: true,
    });

    // this.retornarCoordenadas.emit(coords);

    google.maps.event.addDomListener(marker, 'dragend', (coors: any) => {

      const nuevoMarcador: Coordenada = {
        lat: coors.latLng.lat(),
        lng: coors.latLng.lng(),
      };
      this.retornarCoordenadas.emit(nuevoMarcador);
    });
  }
}
