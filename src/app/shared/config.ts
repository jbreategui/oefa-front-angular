export const WS_LISTAR_EA = 'api/ea/listar';
export const WS_BUSCAR_EA = 'api/ea/buscarEmergencia';


export const WS_RELACIONAR_EMERGENCIA = 'api/ea/relacionarEmergencia';
export const WS_AUTOLOGIN = 'autologin';
export const WS_LISTAR_ESTADOS = 'api/ea/listarEstados';
export const WS_GUARDAR_RPA = 'api/rpea/guardarReportePreliminar';
export const WS_MOSTRAR_RPEA = 'api/rpea/mostrarReportePreliminar';
export const WS_LISTAR_DEPARTAMENTOS = 'api/ubigeo/listarDepartamento';
export const WS_LISTAR_PROVINCIAS = 'api/ubigeo/listarProvincia';
export const WS_LISTAR_DISTRITOS = 'api/ubigeo/listarDistrito';
export const WS_GUARDAR_RFA = 'api/rfea/guardarReportePreliminar';
export const WS_OBTENER_CREDENCIALES = 'archivo/upload';
export const WS_OBTENER_ARCHIVOS_REGISTRADOS = 'archivo';
export const WS_LISTAR_SECTOR = 'api/opcion/sector';
export const WS_LISTAR_UNIDAD_FISCALIZABLE = 'api/opcion/unidadFiscalizable';
export const WS_LISTAR_UNIDAD_MEDIDA = 'api/opcion/unidadMedida';
export const WS_DESCARGAR_ARCHIVO = 'archivo/download';
export const WS_MOSTRAR_RFEA = 'api/rfea/mostrarReporteFinal';
export const WS_REGISTRAR_RFEA = 'api/rfea/guardarReporteFinal';
export const WS_REGISTRAR_RPEA = 'api/rpea/guardarReportePreliminar';
export const WS_REGISTRAR_PRESENTACION_RPEA = 'api/rpea/presentarReportePreliminar';
export const WS_REGISTRAR_PRESENTACION_RFEA = 'api/rfea/presentarReporteFinal';

