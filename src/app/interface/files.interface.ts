export interface FilesAlfresco {
    name: string;
    size: string;
    status: boolean;
    uuid: string;
}
