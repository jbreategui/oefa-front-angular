// Generated by https://quicktype.io

export interface DepartamentoResponse {
    payload: Departamento[];
    estado: boolean;
    status: number;
    mensaje: string;
}

export interface Departamento {
    idDepartamento: string;
    departamentoNombre: string;
}
// Generated by https://quicktype.io

export interface ProvinciaResponse {
    payload: Provincia[];
    estado: boolean;
    status: number;
    mensaje: string;
}

export interface Provincia {
    idProvincia: string;
    provinciaNombre: string;
}
// Generated by https://quicktype.io

export interface DistritoResponse {
    payload: Distrito[];
    estado: boolean;
    status: number;
    mensaje: string;
}

export interface Distrito {
    idDistrito: string;
    distritoNombre: string;
}
