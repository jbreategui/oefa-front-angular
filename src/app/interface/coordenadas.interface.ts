export interface Coordenada {
    lat: number;
    lng: number;
}
export interface Utm {
    easting: number,
    northing: number,
    zoneNum: number,
    zoneLetter: string | null
}

export interface DialogCoordenada {
    coordenadas: Coordenada;
    alturaDialog: string;
}