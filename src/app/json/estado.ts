export const DATA_ESTADO_EA = {
    "estado": true,
    "status": 200,
    "mensaje": "OK",
    "payload": [
        {
            "nombreEstadoEA": "Ocurrencia de EA",
            "descripcionNombreEstadoEA": "Fecha de Ocurrencia de Emergencia Ambiental",
            "estadoEA": true,
            "fechaEstadoEA": "2021-05-06",
            "path": "https://cdn0.iconfinder.com/data/icons/security-double-color-red-and-black-vol-2/52/light__emergency__police__security-256.png"
        },
        {
            "nombreEstadoEA": "Registro en el sistema",
            "descripcionNombreEstadoEA": "Fecha de Ocurrencia de Emergencia Ambiental",
            "estadoEA": true,
            "fechaEstadoEA": "2021-05-06",
            "path": "https://cdn0.iconfinder.com/data/icons/kameleon-free-pack-rounded/110/Download-Computer-256.png"
        },
        {
            "nombreEstadoEA": "Registro de RPEA",
            "descripcionNombreEstadoEA": "Fecha de Ocurrencia de Emergencia Ambiental",
            "estadoEA": true,
            "fechaEstadoEA": "2021-05-06",
            "path": "https://cdn2.iconfinder.com/data/icons/ios-14-custom-application/62/application-57-256.png"
        },
        {
            "nombreEstadoEA": "Registro de RFEA",
            "descripcionNombreEstadoEA": "Fecha de Ocurrencia de Emergencia Ambiental",
            "estadoEA": false,
            "fechaEstadoEA": "2021-05-06",
            "path": "https://cdn3.iconfinder.com/data/icons/education-209/64/paper-clip-academic-note-exam-256.png"
        },
        {
            "nombreEstadoEA": "EA atendida",
            "descripcionNombreEstadoEA": "Fecha de Ocurrencia de Emergencia Ambiental",
            "estadoEA": false,
            "fechaEstadoEA": "2021-05-06",
            "path": "https://cdn0.iconfinder.com/data/icons/infectious-pandemics/480/09-report-256.png"
        }
    ]
}