import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private route: Router) { }
  canActivate(): boolean {
    return this.validarSessionUsuario();
  }

  validarSessionUsuario() {
    const token = localStorage.getItem('sgea-token-cc-wapo');
    if (token !== undefined && token !== null && token !== '') {
      return true;
    } else {
      this.route.navigate(['/auth']);
      return false;
    }

  }

}
