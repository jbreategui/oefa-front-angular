import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageRoutingModule } from './page-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MainComponent } from './main/main.component';
import { BandejaEmergenciaAmbientalComponent } from './bandeja-emergencia-ambiental/bandeja-emergencia-ambiental.component';
import { EstadoEmergenciaAmbientalComponent } from './estado-emergencia-ambiental/estado-emergencia-ambiental.component';
import { ReporteFinalComponent } from './reporte-final/reporte-final.component';
import { ReportePreliminarComponent } from './reporte-preliminar/reporte-preliminar.component';
import { MaterialModule } from '../material/material.module';
import { ComponentsModule } from '../components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthComponent } from './auth/auth.component';
import { MatPaginatorIntl } from '@angular/material/paginator';

@NgModule({
  declarations: [
    MainComponent,
    BandejaEmergenciaAmbientalComponent,
    EstadoEmergenciaAmbientalComponent,
    ReporteFinalComponent,
    ReportePreliminarComponent,
    AuthComponent
  ],
  imports: [
    CommonModule,
    PageRoutingModule,
    FlexLayoutModule,
    PageRoutingModule,
    MaterialModule,
    ComponentsModule,
    ReactiveFormsModule,
    FormsModule,

  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: BandejaEmergenciaAmbientalComponent
    }
  ]
})
export class PageModule { }
