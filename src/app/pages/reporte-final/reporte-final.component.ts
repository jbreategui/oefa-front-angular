import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../shared/shared.service';
import { FilesAlfresco } from '../../interface/files.interface';
import { RPEAService } from '../../services/rpea.service';
import { DomSanitizer } from '@angular/platform-browser';
import { RfeaService } from 'src/app/services/rfea.service';
import { Rfea, DatosRfea, ConsecuenciaRfea, ProductoConsecuencia, MedidaAdoptar, ListaArchivosBloque } from '../../interface/rfea.interface';
import { UnidadMedida } from '../../interface/opcionesDesplegables.interface';
import { ListaArchivo } from '../../interface/rpea.interface';
import { MatDialog } from '@angular/material/dialog';
import { AlertaModalComponent } from '../../components/alerta-modal/alerta-modal.component';

@Component({
  selector: 'app-reporte-final',
  templateUrl: './reporte-final.component.html',
  styleUrls: ['./reporte-final.component.css']
})
export class ReporteFinalComponent implements OnInit {
  files: FilesAlfresco[] = [];
  fileOne: FilesAlfresco[] = [];
  miFormulario!: FormGroup;
  eaId: string = '';
  idRFEA: string = '';
  @ViewChild('fileVisible', { static: false }) fileVisible!: ElementRef;
  @ViewChild('fileVisibleOne', { static: false }) fileVisibleOne!: ElementRef;

  @ViewChild("formUpload", { static: false }) myFormUpload!: ElementRef;

  @ViewChild("formReg", { static: false }) myFormRegister!: ElementRef;
  @ViewChild("formReg2", { static: false }) myFormRegister2!: ElementRef;

  @ViewChild("divContentInputFile", { static: false }) myDivInputFile!: ElementRef;
  @ViewChild("divContentInputFile2", { static: false }) myDivInputFile2!: ElementRef;

  @ViewChild('fileInput1', { static: false }) fileInput1!: ElementRef;
  @ViewChild('fileInput2', { static: false }) fileInput2!: ElementRef;


  hkey = '';
  urlIframe = '';
  idArchivo = 0;
  isLoading = false;
  validarFile: any;
  idFile = 0;
  mensajeCarga = '';
  datosRfea!: Rfea;
  unidadMedidaList: UnidadMedida[] = [];
  flagEnviando = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activated: ActivatedRoute,
    private sharedservice: SharedService,
    private wsrpea: RPEAService,
    private sanitizer: DomSanitizer,
    private wsrfea: RfeaService,
    public dialog: MatDialog,
  ) {
    this.crearFormulario();
  }

  ngOnInit() {

    this.activated.queryParams.subscribe((res) => {
      this.idRFEA = res.idRFEA ? res.idRFEA : '';
      this.eaId = res.idEA ? res.idEA : '';
      this.cargarDatosInicialesSegunIds();

    });
  }
  cargarDatosInicialesSegunIds() {

    if (this.idRFEA !== '' && this.eaId !== '') {
      this.cargarDatosRfea();
    } else {

    }

  }


  cargarDatosRfea() {
    const body = {
      idRFEA: this.idRFEA,
      eaId: this.eaId
    };
    this.wsrfea.obtenerRFEA(body).subscribe((res) => {
      if (res.estado) {
        this.configurarDatosFormulario(res.payload);

        this.listarCOmbosPrincipales(res.payload.consecuenciaRfea, res.payload);

      }

    });
  }

  listarCOmbosPrincipales(item: ConsecuenciaRfea, refea: Rfea) {

    this.wsrfea.listarUnidadMedida().subscribe((res) => {
      if (res.estado) {

        this.unidadMedidaList = res.payload;
        if (item.unidadMedidaId) {
          this.miFormulario.get('unidadMedida')?.setValue(+item.unidadMedidaId);
        }
        this.cargarDatosDerrame(refea.consecuenciaRfea.productoConsecuencia);
        this.cargarDatosMedidaAdoptar(refea.accionCorrectiva.medidaAdoptar);
        this.cargarArchivosRegitradosBloque1(refea.accionCorrectiva.listaArchivosPrimerBloque);
        this.cargarArchivosRegitradosBloque2(refea.accionCorrectiva.listaArchivosSegundoBloque);
      }
    });
  }

  configurarDatosFormulario(item: Rfea) {


    let hoursInicio: any[] = [];
    let hoursFin: any[] = [];
    let inicioValue = null;
    let finValue = null;

    if (item.datosRfea.horaInicioEvento) {
      hoursInicio = item.datosRfea.horaInicioEvento.split(" ");
      inicioValue = hoursInicio[1];
    }

    if (item.datosRfea.horaFinEvento) {
      hoursFin = item.datosRfea.horaFinEvento.split(" ");
      finValue = hoursFin[1];
    }
    this.miFormulario.reset(
      {
        descripcionEvento: item.datosRfea.descripcionEvento,
        causasOrigen: item.datosRfea.causasOrigen,
        condicionesClimaticas: item.datosRfea.condicionesClimaticas,
        flagPlanContingencia: item.datosRfea.flagPlanContingencia,
        descripcionPlanContigencia: item.datosRfea.descripcionPlanContigencia,
        impactoConsecuencia: item.consecuenciaRfea.impactoConsecuencia,
        repercusionConsecuencia: item.consecuenciaRfea.repercusionConsecuencia,
        accionAdministradoConsecuencia: item.consecuenciaRfea.accionAdministradoConsecuencia,
        cantidadMaterialRecuperado: item.consecuenciaRfea.cantidadMaterialNoRecuperado,
        cantidadMaterialNoRecuperado: item.consecuenciaRfea.cantidadMaterialNoRecuperado,
        empresaOperadora: item.accionCorrectiva.empresaOperadora,
        estadoInstalacion: +item.accionCorrectiva.estadoInstalacion,
        horaInicioEvento: inicioValue,
        horaFinEvento: finValue,

      }
    );
  }





  crearFormulario() {
    this.miFormulario = this.formBuilder.group({
      descripcionEvento: [''],
      causasOrigen: [''],
      condicionesClimaticas: [''],
      flagPlanContingencia: [''],
      descripcionPlanContigencia: [''],
      productoConsecuencia: this.formBuilder.array([]),
      impactoConsecuencia: [''],
      repercusionConsecuencia: [''],
      accionAdministradoConsecuencia: [''],
      cantidadMaterialRecuperado: [''],
      unidadMedida: [''],
      cantidadMaterialNoRecuperado: [''],
      empresaOperadora: [''],
      estadoInstalacion: [''],
      horaInicioEvento: [''],
      horaFinEvento: '',
      medidaAdoptar: this.formBuilder.array([]),

    });
  }
  get datosDerrameFuga() {
    return this.miFormulario.get('productoConsecuencia') as FormArray;
  }
  get medidasAdoptar() {
    return this.miFormulario.get('medidaAdoptar') as FormArray;
  }




  cargarDatosDerrame(item: ProductoConsecuencia[]) {


    if (item?.length > 0) {
      item.forEach((r) => {
        this.datosDerrameFuga.push(
          this.formBuilder.group(
            {
              nombreProducto: [r.nombreProducto, Validators.required],
              tipoProducto: [r.tipoProducto, Validators.required],
              galonesVolumen: r.galonesVolumen,
              galonesEspecificar: r.galonesEspecificar,
              areaInvolucrada: [r.areaInvolucrada, Validators.required],
              unidadMedida: [+r.unidadMedidaId, Validators.required],

            }
          )
        );
      });
    } else {
      this.datosDerrameFuga.push(
        this.formBuilder.group(
          {
            nombreProducto: ['', Validators.required],
            tipoProducto: ['', Validators.required],
            galonesVolumen: [''],
            galonesEspecificar: [''],
            areaInvolucrada: ['', Validators.required],
            unidadMedida: [this.unidadMedidaList[0]?.unidadMedidaId, Validators.required],

          }
        )
      );
    }

  }

  cargarDatosMedidaAdoptar(item: MedidaAdoptar[]) {

    if (item?.length > 0) {
      item.forEach((r) => {
        this.medidasAdoptar.push(
          this.formBuilder.group(
            {
              medida: [r.medida, Validators.required],
            }
          )
        );

      });
    } else {
      this.medidasAdoptar.push(
        this.formBuilder.group(
          {
            medida: ['', Validators.required],
          }
        )
      );
    }


  }
  agregarNuevoDatosDerrame() {
    this.datosDerrameFuga.push(this.formBuilder.group(
      {
        nombreProducto: ['', [Validators.required]],
        tipoProducto: ['', [Validators.required]],
        galonesVolumen: [''],
        galonesEspecificar: [''],
        areaInvolucrada: ['', [Validators.required]],
        unidadMedida: [this.unidadMedidaList[0].unidadMedidaId, [Validators.required]],
      }
    ));
  }
  eliminarNuevoDatosDerrame(index: number) {
    this.datosDerrameFuga.removeAt(index);

  }


  agregarNuevaMedidaAdoptar() {
    this.medidasAdoptar.push(this.formBuilder.group(
      {
        medida: ['', [Validators.required]],
      }
    ));
  }
  eliminarNuevoMedidaAdoptar(index: number) {
    this.medidasAdoptar.removeAt(index);

  }
  guardarDatosEF() {
    console.log(this.obtenerDatosForm());
  }
  obtenerDatosForm() {
    return {
      eaID: this.eaId,
      datosRfea: {
        descripcionEvento: this.miFormulario.get('descripcionEvento')?.value,
        causasOrigen: this.miFormulario.get('causasOrigen')?.value,
        condicionesClimaticas: this.miFormulario.get('condicionesClimaticas')?.value,
        flagPlanContingencia: this.miFormulario.get('flagPlanContingencia')?.value,
        descripcionPlanContigencia: this.miFormulario.get('descripcionPlanContigencia')?.value,
        horaInicioEvento: this.miFormulario.get('horaInicioEvento')?.value,
        horaFinEvento: this.miFormulario.get('horaFinEvento')?.value,

      },
      consecuenciaRfea: {
        productoConsecuencia: this.miFormulario.get('productoConsecuencia')?.value,
        impactoConsecuencia: this.miFormulario.get('impactoConsecuencia')?.value,
        repercusionConsecuencia: this.miFormulario.get('repercusionConsecuencia')?.value,
        accionAdministradoConsecuencia: this.miFormulario.get('accionAdministradoConsecuencia')?.value,
        cantidadMaterialRecuperado: this.miFormulario.get('cantidadMaterialRecuperado')?.value,
        unidadMedida: this.miFormulario.get('unidadMedida')?.value,
        cantidadMaterialNoRecuperado: this.miFormulario.get('cantidadMaterialNoRecuperado')?.value,
      },
      accionCorrectiva: {
        listaArchivosPrimerBloque: this.obtenerUUIDfileOne(),
        listaArchivosSegundoBloque: this.obtenerUUIDfiles(),
        medidaAdoptar: this.miFormulario.get('medidaAdoptar')?.value,
        empresaOperadora: this.miFormulario.get('empresaOperadora')?.value,
        estadoInstalacion: this.miFormulario.get('estadoInstalacion')?.value,
      },

    }
  }


  volver() {
    this.router.navigate(['/']);
  }

  limpiar() {

    this.miFormulario.reset({
      descripcionEvento: '',
      causasOrigen: '',
      condicionesClimaticas: '',
      flagPlanContingencia: '',
      descripcionPlanContigencia: '',
      impactoConsecuencia: '',
      repercusionConsecuencia: '',
      accionAdministradoConsecuencia: '',
      cantidadMaterialRecuperado: '',
      unidadMedida: '',
      cantidadMaterialNoRecuperado: '',
      horaInicioEvento: '',
      horaFinEvento: '',
      medidaAdoptar: [],
      empresaOperadora: [''],
      estadoInstalacion: [''],
      productoConsecuencia: []
    });
  }

  fileAlfresco!: FilesAlfresco;
  onFileChangedOne(event: any) {
    this.isLoading = true;
    this.tipoArchivo = 0;
    const file = event.target.files[0];
    const files = [...this.fileOne];
    if (file) {
      const ok = this.sharedservice.validarArchivoPermitido(file, files);
      this.validarFile = ok;
      if (ok.estado) {
        const data = {
          tipoArchivo: "MPV",
          pathAlfresco: "file.path.compromiso.ambiental"
        };
        this.obtenerCredencialesAlfresco(data).subscribe((res: any) => {
          if (res.estado) {
            const fileAlgresco: FilesAlfresco = ok.payload;
            const idarchivo = res.idArchivo;
            const path = res.formActionURL;
            const key = res.keyId;
            this.urlIframe = path;
            this.hkey = key;
            this.idArchivo = idarchivo;
            this.fileAlfresco = fileAlgresco;

            setTimeout(() => {
              this.myFormUpload.nativeElement.appendChild(this.myFormRegister.nativeElement.querySelector('input[type=file]'));
              this.myFormUpload.nativeElement.submit();
            }, 500);
          } else {
            event.target.value = '';
            this.fileVisibleOne.nativeElement.value = '';
            this.fileInput1.nativeElement.value = '';
            this.isLoading = false;
            this.validarFile = null;

          }
        });
      } else {
        event.target.value = '';
        this.fileVisibleOne.nativeElement.value = '';
        this.fileInput1.nativeElement.value = '';
        this.isLoading = false;
      }
    } else {
      event.target.value = '';
      this.fileVisibleOne.nativeElement.value = '';
      this.fileInput1.nativeElement.value = '';
      this.isLoading = false;
      this.validarFile = null;
    }


  }

  obtenerCredencialesAlfresco(body: any) {
    return this.wsrpea.obtenerCredencialesAlfresco(body);
  }




  idArchivo2 = 0;
  fileAlfresco2!: FilesAlfresco;
  validarFile2: any;
  onFileChanged(event: any) {
    this.tipoArchivo = 1;
    this.isLoading = true;
    const file = event.target.files[0];
    const files = [...this.files];
    if (file) {
      const ok = this.sharedservice.validarArchivoPermitido(file, files);
      this.validarFile2 = ok;
      if (ok.estado) {
        const data = {
          tipoArchivo: "MPV",
          pathAlfresco: "file.path.compromiso.ambiental"
        };
        this.obtenerCredencialesAlfresco(data).subscribe((res: any) => {
          if (res.estado) {
            const fileAlgresco: FilesAlfresco = ok.payload;
            const idarchivo = res.idArchivo;
            const path = res.formActionURL;
            const key = res.keyId;
            this.urlIframe = path;
            this.hkey = key;
            this.idArchivo2 = idarchivo;
            this.fileAlfresco2 = fileAlgresco;
            setTimeout(() => {
              this.myFormUpload.nativeElement.appendChild(this.myFormRegister2.nativeElement.querySelector('input[type=file]'));
              this.myFormUpload.nativeElement.submit();
            }, 500);
          } else {
            event.target.value = '';
            this.fileVisible.nativeElement.value = '';
            this.fileInput2.nativeElement.value = '';
            this.isLoading = false;
            this.validarFile2 = null;

          }
        });
      } else {
        event.target.value = '';
        this.fileVisible.nativeElement.value = '';
        this.fileInput2.nativeElement.value = '';
        this.isLoading = false;

      }
    } else {
      event.target.value = '';
      this.fileVisible.nativeElement.value = '';
      this.fileInput2.nativeElement.value = '';
      this.isLoading = false;
      this.validarFile2 = null;
    }

  }


  obtenerUUIDfiles() {
    const filtrarFiles = this.files
      .filter(r => r.status === true)
      .map((r) => {
        return {
          uuidArchivo: r.uuid
        }
      });
    return filtrarFiles;

  }

  obtenerUUIDfileOne() {
    const filtrarFiles = this.fileOne
      .filter(r => r.status === true)
      .map((r) => {
        return {
          uuidArchivo: r.uuid
        }
      });
    return filtrarFiles;

  }

  tipoArchivo = 0;

  iframeLoaded() {


    if (this.tipoArchivo === 0) {
      if (this.idArchivo === 0) {
        return;
      }
      this.validarArchivo1();
    }
    if (this.tipoArchivo === 1) {
      if (this.idArchivo2 === 0) {
        return;
      }
      this.validarArchivo2();

    }




  }
  validarArchivo1() {

    this.myDivInputFile.nativeElement.appendChild(this.fileInput1.nativeElement);
    this.fileInput1.nativeElement.value = '';
    this.wsrpea.obtenerArchivoRegistrado(this.idArchivo).subscribe((res: any) => {
      this.fileAlfresco.uuid = res.uuid;
      this.fileAlfresco.status = true;
      this.isLoading = false;
      //   this.fileVisibleOne.nativeElement.value = this.fileAlfresco.name;
      this.fileAlfresco.name = res?.nombreArchivo;
      this.fileOne.push({ ...this.fileAlfresco });

    }, () => {
      this.isLoading = false;
    });
  }
  validarArchivo2() {
    this.myDivInputFile2.nativeElement.appendChild(this.fileInput2.nativeElement);
    this.fileInput2.nativeElement.value = '';
    this.wsrpea.obtenerArchivoRegistrado(this.idArchivo2).subscribe((res: any) => {
      this.fileAlfresco2.uuid = res.uuid;
      this.fileAlfresco2.status = true;
      // this.fileVisible.nativeElement.value = this.fileAlfresco2.name;
      this.fileAlfresco2.name = res?.nombreArchivo;
      this.isLoading = false;
      this.files.push({ ...this.fileAlfresco2 });
    }, () => {
      this.isLoading = false;
    });
  }
  iframeURL(url: any) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  getIframeHTML() {
    const element: HTMLIFrameElement = document.getElementById('uploadiframe') as HTMLIFrameElement;
    let algo = element.contentDocument ? element.contentDocument : element.contentWindow;
  }

  descargarArchivoGenerado(item: FilesAlfresco) {
    this.wsrpea.descargarArchivo(item.uuid).subscribe((res) => {

      const a = document.createElement('a');
      a.href = res.fullUrlDownload;
      a.target = '_BLANK';
      a.click();

    });
  }

  cargarArchivosRegitradosBloque1(item: ListaArchivosBloque[]) {
    item.forEach((r, i) => {

      if (r) {

        r.nombreArchivo = r?.nombreArchivo ? r?.nombreArchivo : 'documento_sin_nombre_' + (i + 1),
          this.files.push(
            {
              name: r?.nombreArchivo,
              uuid: r?.uuidArchivo,
              size: '',
              status: true,

            }
          );
      }

    });
  }
  cargarArchivosRegitradosBloque2(item: ListaArchivosBloque[]) {
    item.forEach((r, i) => {

      if (r) {

        r.nombreArchivo = r?.nombreArchivo ? r?.nombreArchivo : 'documento_sin_nombre_' + (i + 1),
          this.files.push(
            {
              name: r?.nombreArchivo,
              uuid: r?.uuidArchivo,
              size: '',
              status: true,

            }
          );
      }

    });
  }


  guardarPresntacionDialog(mensaje: string) {
    const dialogRef = this.dialog.open(AlertaModalComponent, {
      width: '700px',
      height: 'auto',
      panelClass: 'mat-sgea-dialog',
      data: mensaje
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {

      }
    });

  }

  registrarPresentacion() {
    this.flagEnviando = true;
    this.wsrfea.registrarPresentacionrfea({ eaId: this.eaId }).subscribe((res: any) => {
      this.flagEnviando = false;
      this.guardarPresntacionDialog(res.mensaje);

    }, () => {
      this.flagEnviando = false;
      this.guardarPresntacionDialog('Hubo un error al enviar los datos');
    });
  }

  registrarRFEA() {
    this.flagEnviando = true;
    const rfea = this.obtenerDatosForm();
    this.wsrfea.registrarRFEA({ ...rfea }).subscribe((res: any) => {
      this.flagEnviando = false;
      this.guardarPresntacionDialog(res.mensaje);
    }, () => {
      this.flagEnviando = false;
      this.guardarPresntacionDialog('Hubo un error al enviar los datos');
    });
  }
}
