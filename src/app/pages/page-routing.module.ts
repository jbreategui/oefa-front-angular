import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { BandejaEmergenciaAmbientalComponent } from './bandeja-emergencia-ambiental/bandeja-emergencia-ambiental.component';
import { EstadoEmergenciaAmbientalComponent } from './estado-emergencia-ambiental/estado-emergencia-ambiental.component';
import { ReportePreliminarComponent } from './reporte-preliminar/reporte-preliminar.component';
import { ReporteFinalComponent } from './reporte-final/reporte-final.component';
import { AuthGuard } from '../guards/auth.guard';
import { AuthComponent } from './auth/auth.component';

const routes: Routes = [
  {
    path: '', component: MainComponent, children: [

      {
        path: 'auth', component: AuthComponent,

      },
      {
        path: 'bandeja', canActivate: [AuthGuard], component: BandejaEmergenciaAmbientalComponent,

      },
      {
        path: 'estado/:id', canActivate: [AuthGuard], component: EstadoEmergenciaAmbientalComponent
      },
      {
        path: 'reporte-pre', canActivate: [AuthGuard], component: ReportePreliminarComponent
      },
      {
        path: 'reporte-fin', canActivate: [AuthGuard], component: ReporteFinalComponent
      },
      {
        path: '**', redirectTo: 'bandeja'
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
