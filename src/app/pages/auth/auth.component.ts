import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  mensaje = 'El usuario no está autenticado';
  load = false;
  constructor(private router: Router,
    private actRoute: ActivatedRoute,
    private loginws: LoginService
  ) {
    this.load = true;
    localStorage.removeItem('sgea-token-cc-wapo');
    this.actRoute.queryParams.subscribe(params => {

      const token = params['token']
      if (token != null) {
        this.mensaje = 'Validando credenciales';
        this.loginws.autologin(token).subscribe((res: any) => {
          this.load = false;
          if (res.payload) {
            localStorage.setItem('sgea-login-cliente', JSON.stringify(res));
            localStorage.setItem('sgea-token-cc-wapo', res.payload);
            this.router.navigate(['/bandeja']);
          } else {
            this.mensaje = res.mensaje;
          }

        }, () => {
          this.mensaje = 'Error al conectar con el servidor';
          this.load = false;
        });
      } else {
        this.load = false;
      }


    });
  }

  ngOnInit(): void {

  }

}
